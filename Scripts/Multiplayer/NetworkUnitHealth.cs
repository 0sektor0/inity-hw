﻿using UnityEngine;
using UnityEngine.Networking;



public class NetworkUnitHealth : NetworkBehaviour, IWoundable
{
    public event DeathDelegate OnUnitDeath;
    public float maxHealth = 100;
    public ParticleSystem explosionPrefab;
    [SyncVar] private float _currentHealth = 100;
    [SyncVar] private bool isInvulnerable = false;


    private void Start()
    {
        SetHp(maxHealth);
        Debug.Log("UnitHealth started");
    }


    private void Update()
    {
        if (_currentHealth <= 0)
            SelfDestroy();
    }


    public void DealDamage(Damage dmg)
    {
        if(dmg != null)
            SetHp(_currentHealth - dmg.value);
    }


    public float GetHpPercent()
    {
        return _currentHealth / maxHealth;
    }


    public float GetHp()
    {
        return _currentHealth;
    }


    public float GetMaxHp()
    {
        return maxHealth;
    }


    public void SetHp(float value)
    {
        if (!isServer)
            return;

        if (value <= 0)
            SelfDestroy();

        if (value > maxHealth)
            value = maxHealth;

        _currentHealth = value;
    }


    public void SetMaxHp(float value)
    {
        if (!isServer)
            return;

        if (value > 0)
            maxHealth = value;
        else
            Debug.LogError("maxHp cannot be less then 0");
    }


    public void RestoreHealth(float heal)
    {
        SetHp(_currentHealth + heal);
    }


    public virtual bool IsAlive()
    {
        if (_currentHealth > 0)
            return true;

        return false;
    }


    protected virtual void SelfDestroy()
    {
        var explosion = Instantiate(explosionPrefab, transform.position, transform.rotation);
        explosion.Play();
        Destroy(gameObject);
    }

    
    public void SetInvulnerability(bool enabled)
    {
        if(isServer)
            isInvulnerable = enabled;
    }


    public bool IsInvulnerable()
    {
        return isInvulnerable;
    }
    

    public virtual void AddOnDeathEvent(DeathDelegate del)
    {
        OnUnitDeath += del;
    }

}
