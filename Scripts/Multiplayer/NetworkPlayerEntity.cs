﻿using System;
using UnityEngine;
using UnityEngine.Networking;



public class NetworkPlayerEntity : NetworkBehaviour
{
    public GameObject unitPrefab;
    static public NetworkPlayerEntity singltone;


    private void Start()
    {
        singltone = this;

        if(!isServer)
            CmdSpawnUnit();
    }


    [Command]
    public void CmdSpawnUnit()
    {
        var go = Instantiate(unitPrefab, transform.transform.position, transform.rotation);
        NetworkServer.SpawnWithClientAuthority(go, connectionToClient);
    }
}