﻿using UnityEngine;
using UnityEngine.Networking;



public class NetworkTurretShoot : NetworkBehaviour, IShootable
{
    public float reloadingTime = 1;
    public Transform shellSpawn;        //the object from which we shoot
    public Rigidbody shellPrefab;       //the object which will shoot
    protected float nextShoot = 0.0f;
    [SyncVar] public float power = 200;
    [SyncVar] private int ammoCapacity = 20;
    [SerializeField] float shellDamage = 35;
    private Damage dmg;


    private void Start()
    {
        ChangeDamage(shellDamage);
        Debug.Log("TurretShoot started");
    }


    public void ChangeDamage(float shellDamage)
    {
        dmg = new Damage(shellDamage, gameObject.GetComponent<ITeamable>());
    }


    public void Shoot()
    {
        CmdFire();
        Reload();
    }


    public void ChangePower(float value)
    {
        if (!isServer)
            return;

        power = value;
    }


    public float GetReloadingPercent()
    {
        float remainingTime = nextShoot - Time.time;
        if (remainingTime < 0)
            remainingTime = 0;

        return 1 - remainingTime / reloadingTime;
    }


    public bool IsReloading()
    {
        return nextShoot > 0;
    }


    [Command]
    private void CmdFire()
    {
        if (ammoCapacity != 0 && Time.time > nextShoot)
        {
            var shell = Instantiate(shellPrefab, shellSpawn.position, shellSpawn.rotation);
            ShellDamage sd = shell.GetComponent<ShellDamage>();

            //set unitIdentityInfo as shell owner 
            if (sd != null)
            {
                shell.velocity = (shellSpawn.forward * power);
                sd.SetDamage(dmg);
            }
            else
                Debug.LogError("Shell prefab must contain ShellDamage component");

            Reload();
            NetworkServer.Spawn(shell.gameObject);
        }
    }


    public void Reload()
    {
        //ammoCapacity--;
        nextShoot = Time.time + reloadingTime;
    }


    public int GetAmmoCount()
    {
        return ammoCapacity;
    }


    public void AddAmmo(int count)
    {
        ammoCapacity += count;
    }


    public void AddDamage(float additionalDmg)
    {
        ChangeDamage(dmg.value + additionalDmg);
    }


    public void ReduceReloading(float additionalSpeed)
    {
        reloadingTime -= additionalSpeed;
        if (reloadingTime < 0)
            reloadingTime = 0;
    }
}
