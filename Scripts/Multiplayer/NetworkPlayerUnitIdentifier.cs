﻿using UnityEngine;
using UnityEngine.Networking;



public class NetworkPlayerUnitIdentifier : NetworkBehaviour
{
    private bool isPlayer = true;


    public override void OnStartAuthority()
    {
        gameObject.AddComponent<PlayerHud>();
        return;
    }


    private void Start()
    {
        if (hasAuthority)
            return;

        isPlayer = false;

        //для неиграбельных вражеских юнитов отключаем все возможности 
        //влияния на них
        Destroy(GetComponent<TankControls>());
        Destroy(GetComponent<TurretMouseControls>());
        Destroy(GetComponent<NetworkPlayerCameraManager>());
        gameObject.AddComponent<OnHeadHealthBar>();
        Destroy(this);
    }


    private void OnDestroy()
    {
        if(isPlayer)
            NetworkPlayerEntity.singltone.CmdSpawnUnit();
    }
}
