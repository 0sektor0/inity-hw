﻿using UnityEngine;



public delegate void DeathDelegate(ITeamable victim, ITeamable killer);



//любой юнит который может получать дамаг, в основном будет вызываться IShootable для получения дамага
//интерфейс выбран потому что в мультиплеере придется использовать сетевые скрипты и синхронизации хп
//простым наследованиием от скрипта для синглового юнита тут не отделаешься
public interface IWoundable
{
    void DealDamage(Damage dmg);

    bool IsAlive();

    float GetHp();

    float GetHpPercent();

    float GetMaxHp();

    void SetHp(float value);

    void SetMaxHp(float value);

    void SetInvulnerability(bool enabled);

    bool IsInvulnerable();

    void AddOnDeathEvent(DeathDelegate del);
}



//описывает команду в который состоит юнит или его снаряд, будет использоваться для подсчет киллоа определенным юнитом 
//по тем же причинам, что и в прошлые разы, а именно необходдимость в синхронизации значений в сетевых 
//скриптах лучше использовать интерфейс, а не наследоваться от сингловой версии
public interface ITeamable
{
    int GetUnitId();

    string GetUnitName();

    int GetTeamId();

    string GetTeamName();

    int GetUnitWorth();
}



//реализация стрельбы будет отличаться у сетевых и сингловых реализаций, сеиевые будут использовать 
//серверные комманды для спауна снарядов 
public interface IShootable
{
    void Shoot();

    int GetAmmoCount();

    void AddAmmo(int count);

    void Reload();

    bool IsReloading();

    float GetReloadingPercent();

    void ChangePower(float value);

    void ChangeDamage(float dmg);

    void AddDamage(float dmg);

    void ReduceReloading(float speed);
}



public interface ISwapableCamera
{
    void SwapCameras();
}



public interface IBuffable
{
    void UseBuff(Buff buff);
}



public interface IGameManager
{
    IWoundable[] GetEnemies();
}