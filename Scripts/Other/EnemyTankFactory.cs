﻿using System.Collections;
using UnityEngine;



//respawn dead units
class EnemyTankFactory : MonoBehaviour
{
    public SinglePlayerGameManager gameManager;
    public Transform[] patroolPoints;
    public Transform spawn;
    public GameObject prefab;


    private IEnumerator Start()
    {
        IWoundable[] units = gameManager.GetEnemies();
        while(units == null)
        {
            units = gameManager.GetEnemies();
            yield return null;
        }

        foreach (IWoundable unit in units)
        {
            unit.AddOnDeathEvent(Respawn);
            yield return null;
        }
    }


    private void Respawn(ITeamable enemy, ITeamable player)
    {
        var go = Instantiate(prefab, spawn.position, spawn.rotation);
        var patrooling = go.GetComponent<Patroling>();
        var health = go.GetComponent<IWoundable>();

        if (health != null)
            health.AddOnDeathEvent(Respawn);

        if (patrooling != null)
            patrooling.points = patroolPoints;
    }
}