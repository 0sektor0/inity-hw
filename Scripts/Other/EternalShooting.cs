﻿using UnityEngine;



[RequireComponent(typeof(IShootable))]
public class EternalShooting : MonoBehaviour
{
    IShootable turret;

    private void Start()
    {
        turret = GetComponent<IShootable>();
    }


    private void Update()
    {
        turret.Shoot();
        turret.AddAmmo(1);
    }
}