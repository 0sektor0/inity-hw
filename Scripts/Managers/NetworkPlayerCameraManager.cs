﻿using UnityEngine;
using UnityEngine.Networking;




public class NetworkPlayerCameraManager : NetworkBehaviour, ISwapableCamera
{
    private void Update()
    {
        if (!(hasAuthority || isLocalPlayer))
            return;

        Vector3 v = gameObject.transform.position;
        Vector3 cameraPosition = Camera.main.transform.position;
        v.Set(v.x, cameraPosition.y, v.z);
        Camera.main.transform.position = v;
    }


    public void SwapCameras()
    {

    }
}
