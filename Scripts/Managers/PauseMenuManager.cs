﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;



public class PauseMenuManager : MonoBehaviour
{
    public Text titleText;
    private bool isPause = true;
    public CanvasGroup pauseUi;


    private void Start()
    {
        PauseGame();
    }


    private void Update()
    {
        if (Input.GetButtonDown("Cancel") && enabled)
            PauseGame();
    }


    public void ExitToMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }


    public void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }


    public void PauseGame()
    {
        if (isPause)
        {
            Time.timeScale = 1;
            pauseUi.alpha = 0;
            pauseUi.interactable = false;
            isPause = false;
        }
        else
        {
            Time.timeScale = 0;
            pauseUi.alpha = 1;
            pauseUi.interactable = true;
            isPause = true;
        }
    }


    public void ChangeTitle(string title)
    {
        titleText.text = title;
    }
}
