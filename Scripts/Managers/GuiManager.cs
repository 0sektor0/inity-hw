﻿using UnityEngine;



//описание доступной гуишки со всеми ее компонентами
public class Gui
{
    public RectTransform canvasTransform;
    public PauseMenuManager pauseMaanger;
    public PlayerHudUpdator hud;


    public Gui(GameObject go)
    {
        pauseMaanger = go.GetComponent<PauseMenuManager>();
        canvasTransform = go.GetComponent<RectTransform>();
        hud = go.GetComponent<PlayerHudUpdator>();

        if (!iscorrect)
            Debug.LogError("guiPrefab does not contain Canvas or PlayerHud");
        else
            Debug.Log("Gui added");
    }


    public bool iscorrect
    {
        get
        {
            return !(canvasTransform == null || hud == null || pauseMaanger == null);
        }
    }
}



//'деревенский' синглтон для полкчения текущего гуи 
public class GuiManager : MonoBehaviour
{
    public GameObject guiPrefab;
    static private GameObject _guiPrefab;
    static private Gui _gui;


    private void Start()
    {
        if(_gui != null)
        {
            Debug.LogError("gui already defined");
            return;
        }

        if (guiPrefab == null)
        {
            enabled = false;
            Debug.LogError("gui prefab is not seted");
        }

        _guiPrefab = guiPrefab;
        checkInstance();
    }


    static private void checkInstance()
    {
        if (_gui == null && _guiPrefab != null)
            _gui = new Gui(Instantiate(_guiPrefab));
    }


    static public Gui singleton
    {
        get
        {
            checkInstance();
            return _gui;
        }
    }


    private void OnDestroy()
    {
        _gui = null;
    }
}
