﻿using UnityEngine;



//класс для переключения между двумя камерами на танке
public class PlayerCameraManager : MonoBehaviour, ISwapableCamera
{
    public Camera topdownCamera;
    public Camera thirdpersonCamera;
    private Camera defaultCamera;
    private Quaternion topdownCameraRotation;
    private bool topdownMode = true;
    private string mainCameraTag = "MainCamera";
    
    
    protected virtual void Start()
    {
        defaultCamera = Camera.main;
        defaultCamera.enabled = false;

        topdownCameraRotation = topdownCamera.transform.rotation;
        topdownCamera.tag = mainCameraTag;
        thirdpersonCamera.tag = mainCameraTag;

        thirdpersonCamera.enabled = false;
        topdownCamera.enabled = true;

        Debug.Log("PlayerCameraManager started");
    }


    protected virtual void Update()
    {
        FixTopdownCameraRotation();

        if (Input.GetButtonDown("SwapCamera1"))
            SwapCameras();
    }


    private void FixTopdownCameraRotation()
    {
        if (topdownMode)
            topdownCamera.transform.rotation = topdownCameraRotation;
    }


    public void SwapCameras()
    {
        topdownMode = !topdownMode;
        thirdpersonCamera.enabled = !thirdpersonCamera.enabled;
        topdownCamera.enabled = !topdownCamera.enabled;

        Debug.Log("Cameras swaped");
    }


    private void OnDestroy()
    {
        if (defaultCamera == false)
            return;

        defaultCamera.enabled = true;
        Destroy(topdownCamera); ;
        Destroy(thirdpersonCamera);
    }
}
