﻿using UnityEngine;
using System.Collections;



public class SinglePlayerGameManager : MonoBehaviour, IGameManager
{
    public UnitHealth player;
    protected int enemiesCount;
    protected int score = 0;
    protected PauseMenuManager pauseManager;
    protected EnemyHealth[] enemies;


    protected virtual void Start()
    {
        //из мэнеджера гуишки берем мэнэджер паузы
        StartCoroutine(GetPauseManager());

        enemies = FindObjectsOfType<EnemyHealth>();
        enemiesCount = enemies.Length;
        StartCoroutine(SetDeathNotification(enemies));

        player.OnUnitDeath += NotifyAboutPlayerDeath;
    }

    
    public IWoundable[] GetEnemies()
    {
        return enemies;
    }


    private IEnumerator GetPauseManager()
    {
        Gui gui = null;

        while(gui == null)
        {
            gui = GuiManager.singleton;
            yield return null;
        }

        pauseManager = GuiManager.singleton.pauseMaanger;
    }
    

    private IEnumerator SetDeathNotification(EnemyHealth[] enemies)
    {
        foreach (EnemyHealth enemy in enemies)
        {
            enemy.OnUnitDeath += NotifyAboutEnemyDeath;
            yield return null;
        }
    }


    protected virtual void EndGame()
    {
        if (pauseManager != null)
            if(pauseManager.enabled)
            {
                if (enemiesCount == 0 && player != null)
                    pauseManager.ChangeTitle(string.Format("Victory\nScore: {0}", score));
                else
                    pauseManager.ChangeTitle(string.Format("You died\nScore: {0}", score));

                pauseManager.enabled = false;
                pauseManager.PauseGame();
            }
    }


    private void NotifyAboutEnemyDeath(ITeamable enemy, ITeamable player)
    {
        enemiesCount--;
        score += enemy.GetUnitWorth();

        if (enemiesCount <= 0)
            EndGame();
    }


    private void NotifyAboutPlayerDeath(ITeamable enemy, ITeamable player)
    {
        player = null;
        EndGame();
    }
}
