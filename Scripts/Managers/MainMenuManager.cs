﻿using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;



[RequireComponent(typeof(Text))]
public class MainMenuManager : MonoBehaviour
{
    public Text progressBar;
    public CanvasGroup mainGroup;
    public CanvasGroup levelSelectionGroup;


    public void Start()
    {
        progressBar = GetComponent<Text>();
    }


    public void LoadSinglePlayer()
    {
        //string sceneName = GetComponent<Text>().text;
        string sceneName = "TankTutorial";
        StartCoroutine(LoadScene(sceneName));
    }


    private IEnumerator LoadScene(string scene)
    {
        var loadSceneOperation = SceneManager.LoadSceneAsync(scene);

        while (!loadSceneOperation.isDone)
        {
            progressBar.text = string.Format("{0}%", (int)loadSceneOperation.progress);
            yield return null;
        }

        progressBar.text = "";
    }


    public void StartMultyplayer()
    {
        StartCoroutine(LoadScene("Multiplayer"));
    }


    public void StartTest()
    {
        StartCoroutine(LoadScene("Test"));
    }


    public void StartBaseDestruction()
    {
        StartCoroutine(LoadScene("BaseDestruction"));
    }


    public void ExitFromGame()
    {
        Application.Quit();
    }


    private void SwitchCanvasGroupState(CanvasGroup group)
    {
        group.alpha = 1 - group.alpha;
        group.interactable = !group.interactable;
        group.blocksRaycasts = !group.blocksRaycasts;
    }


    public void SwitchLayout()
    {
        SwitchCanvasGroupState(mainGroup);
        SwitchCanvasGroupState(levelSelectionGroup);
    }
}
