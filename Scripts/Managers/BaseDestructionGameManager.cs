﻿using System;



public class BaseDestructionGameManager : SinglePlayerGameManager
{
    public UnitHealth enemyBase;


    protected override void Start()
    {
        base.Start();
        enemyBase.AddOnDeathEvent(NotifyAbouBaseDeath);
    }


    private void NotifyAbouBaseDeath(ITeamable enemy, ITeamable player)
    {
        enemiesCount--;
        score += enemy.GetUnitWorth();
        enemyBase = null;
        
        EndGame();
    }


    protected override void EndGame()
    {
        if (pauseManager != null)
            if (pauseManager.enabled)
            {
                if (enemyBase == null && player != null)
                    pauseManager.ChangeTitle(string.Format("Victory\nScore: {0}", score));
                else
                    pauseManager.ChangeTitle(string.Format("You died\nScore: {0}", score));

                pauseManager.enabled = false;
                pauseManager.PauseGame();
            }
    }
}