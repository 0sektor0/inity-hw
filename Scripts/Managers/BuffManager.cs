﻿using UnityEngine;
using System.Collections.Generic;



public class BuffManager : MonoBehaviour, IBuffable
{
    public int buffsCountLimit = 10;
    private Stack<Buff> buffsStack;


    private void Start()
    {
        buffsStack = new Stack<Buff>(buffsCountLimit);
    }


    public void UseBuff(Buff buff)
    {
        if (buffsStack.Count < buffsCountLimit)
            buffsStack.Push(buff);
    }


    private void Update()
    {
        int count = buffsStack.Count;

        for(int i=0; i<count; i++)
        {
            //применяем бафф и если еще можно применять, то возвращаем обратно в стэк
            Buff buff = buffsStack.Pop();

            if (buff.ApplyBuff())
                buffsStack.Push(buff);
        }
    }
}
