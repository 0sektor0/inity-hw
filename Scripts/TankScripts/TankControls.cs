﻿using UnityEngine;



public class TankControls : MonoBehaviour
{
    protected TankMovements tm;
    protected IShootable turret;
    protected float movementAxis;
    protected float rotationAxis;


    protected void Start ()
    {
        tm = GetComponent<TankMovements>();
        turret = GetComponent<IShootable>();
        Debug.Log("TankControls started");
    }


    protected virtual void Update()
    {
        //Debug.Log(GetComponent<Rigidbody>().velocity);
        movementAxis = Input.GetAxis("Vertical1");
        rotationAxis = Input.GetAxis("Horizontal1");
    }


    protected virtual void FixedUpdate()
    {
        if (tm.CanMove())
        {
            tm.Move(movementAxis);
            tm.Turn(rotationAxis);
        }
        
        if ((Input.GetButtonDown("Fire1") || Input.GetMouseButtonDown(0)))
            turret.Shoot();

        tm.LiftMuzzle(Input.GetAxis("Vertical2"));
    }
}
