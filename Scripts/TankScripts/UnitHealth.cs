﻿using System;
using System.Collections;
using UnityEngine;



[RequireComponent(typeof(ITeamable))]
public class UnitHealth : MonoBehaviour, IWoundable
{
    public event DeathDelegate OnUnitDeath;
    [SerializeField] private float maxHealth;
    [SerializeField] private ParticleSystem explosionPrefab;
    [SerializeField] private GameObject wreckagePrefab;
    [SerializeField] private bool isInvulnerable = false;
    private ITeamable killerIdentityInfo;
    private ITeamable unitIdentityInfo;
    private float currentHealth;


    private void Start()
    {
        SetHp(maxHealth);
        unitIdentityInfo = gameObject.GetComponent<ITeamable>();

        Debug.Log("UnitHealth started");
    }


    public void DealDamage(Damage dmg)
    {
        if (dmg.value > 0)
        {
            if (isInvulnerable)
                return;

            killerIdentityInfo = dmg.owner;
        }

        SetHp(currentHealth - dmg.value);
    }


    public float GetHpPercent()
    {
        return currentHealth / maxHealth;
    }


    public float GetHp()
    {
        return currentHealth;
    }


    public float GetMaxHp()
    {
        return maxHealth;
    }


    public void SetHp(float value)
    {
        if (value <= 0)
            SelfDestroy();

        if (value > maxHealth)
            value = maxHealth;

        currentHealth = value;
    }


    public void SetMaxHp(float value)
    {
        if (value > 0)
            maxHealth = value;
        else
            Debug.LogError("maxHp cannot be less then 0");
    }
    

    public virtual bool IsAlive()
    {
        if (currentHealth > 0)
            return true;

        return false;
    }

    
    protected virtual void SelfDestroy()
    {
        //notifying game manager abou unit death, 
        //for score calculating and game managing
        if(OnUnitDeath != null)
            OnUnitDeath(unitIdentityInfo, killerIdentityInfo);

        //destroying unit and spawn his wreckages
        var explosion = Instantiate(explosionPrefab, transform.position, transform.rotation);
        explosion.Play();

        Vector3 wreckagePosition = new Vector3(transform.position.x, 0, transform.position.z);
        var wreckage = Instantiate(wreckagePrefab, wreckagePosition, transform.rotation);

        Destroy(gameObject);
    }


    public virtual void AddOnDeathEvent(DeathDelegate del)
    {
        OnUnitDeath += del;
    }


    public void SetInvulnerability(bool enabled)
    {
        isInvulnerable = enabled;
    }


    public bool IsInvulnerable()
    {
        return isInvulnerable;
    }
}
