﻿using UnityEngine;


public class TeamIdentity : MonoBehaviour, ITeamable
{
    [SerializeField] int teamId;
    [SerializeField] int unitId;
    [SerializeField] string teamName;
    [SerializeField] string unitName;
    [SerializeField] int unitWorth;


    public int GetUnitId()
    {
        return unitId;
    }

    public string GetUnitName()
    {
        return unitName;
    }

    public int GetTeamId()
    {
        return teamId;
    }

    public string GetTeamName()
    {
        return teamName;
    }

    public int GetUnitWorth()
    {
        return unitWorth;
    }
}
