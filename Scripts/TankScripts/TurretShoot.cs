﻿using UnityEngine;



[RequireComponent(typeof(ITeamable))]
public class TurretShoot : MonoBehaviour, IShootable
{
    public float reloadingTime = 1;
    public Transform shellSpawn;        //the object from which we shoot
    public Rigidbody shellPrefab;       //the object which will shoot
    private float nextShoot = 0.0f;
    private Damage dmg;
    [SerializeField] private int ammoCapacity = 20;
    [SerializeField] float power = 100;
    [SerializeField] float shellDamage = 35;


    private void Start()
    {
        ChangeDamage(shellDamage);
        Debug.Log("TurretShoot started");
    }


    public void ChangeDamage(float shellDamage)
    {
        dmg = new Damage(shellDamage, gameObject.GetComponent<ITeamable>());
    }


    public void Shoot()
    {
        if (ammoCapacity != 0 && Time.time > nextShoot)
        {
            var shell = Instantiate(shellPrefab, shellSpawn.position, shellSpawn.rotation);
            ShellDamage sd = shell.GetComponent<ShellDamage>();

            //set unitIdentityInfo as shell owner 
            if (sd != null)
                sd.SetDamage(dmg);
            else
                Debug.LogError("Shell prefab must contain ShellDamage component");

            shell.AddForce(shellSpawn.forward * power);
            Reload();
        }
    }


    public void Reload()
    {
        ammoCapacity--;
        nextShoot = Time.time + reloadingTime;
    }


    public int GetAmmoCount()
    {
        return ammoCapacity;
    }


    public void AddAmmo(int count)
    {
        ammoCapacity += count;
    }


    public void ChangePower(float value)
    {
        if (value >= 0)
            power = value;
    }

    
    public float GetReloadingPercent()
    {
        float remainingTime = nextShoot - Time.time;
        if (remainingTime < 0)
            remainingTime = 0;

        return 1 - remainingTime / reloadingTime; 
    }


    public bool IsReloading()
    {
        return nextShoot > 0;
    }


    public void AddDamage(float additionalDmg)
    {
        ChangeDamage(dmg.value + additionalDmg);
    }


    public void ReduceReloading(float additionalSpeed)
    {
        reloadingTime -= additionalSpeed;
        if (reloadingTime < 0)
            reloadingTime = 0;
    }
}
