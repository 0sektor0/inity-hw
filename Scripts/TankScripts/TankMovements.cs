﻿using UnityEngine;



public class TankMovements : MonoBehaviour
{
    public float force = 1000;
    public float maxSpeed = 5;
    public float stopTilt = 90;
    public float movementSpeed = 4;
    public float tankRotationSpeed = 40;
    public float reverseGearSlowdownRate = 2;
    public float muzzleRotationSpeed = 0.1f;
    public float turretRotationAngle = 1;
    public Transform turret;           //turret may contain shell spawn object with turretShoot script
    public Transform turretMuzzle;     //the object from which we shoot
    private Rigidbody body;

    private const float maxDistanceAboveGround = 0.4f;
    private const float grondedRaycastOffset = 0.1f;


    private void Start()
    {
        body = GetComponent<Rigidbody>();
        Debug.Log("TankMovement started");
    }


    public void Move(float movementAxis)
    {
        float speed = body.velocity.magnitude;

        if (speed >= maxSpeed || movementAxis == 0)
            return;

        Vector3 shift = transform.forward * force * movementAxis;

        if (movementAxis < 0)
            shift = shift / reverseGearSlowdownRate;

        //body.MovePosition(body.position + shift);
        //Debug.Log(body.velocity.magnitude);
        body.AddForce(transform.forward * force * movementAxis);
    }


    public void IncreaseMobility(float maxSpeedAddition, float turnSpeedAddition, float turretSpeedAddition)
    {
        maxSpeed += maxSpeedAddition;
        tankRotationSpeed += turnSpeedAddition;
        turretRotationAngle += turretSpeedAddition;
    }


    public void Turn(float rotationAxis)
    {
        var turn = rotationAxis * tankRotationSpeed * Time.deltaTime;
        var turnY = Quaternion.Euler(0, turn, 0);

        body.MoveRotation(body.rotation * turnY);
    }


    public bool CanMove()
    {
        //if (!IsGrounded())
        //    return false;

        //да, 360 это магическое число, но его смысл и так ясен
        if (transform.rotation.eulerAngles.z < stopTilt || transform.rotation.eulerAngles.z > 360 - stopTilt)
            return true;

        return false;
    }


    private bool IsGrounded()
    {
        var start = transform.position + new Vector3(0, grondedRaycastOffset, 0);
        var direction = transform.TransformDirection(Vector3.down);

        Debug.DrawRay(start, direction * maxDistanceAboveGround, Color.red);
        return Physics.Raycast(start, direction, maxDistanceAboveGround);
    }


    public void RotateTurret(float angle)
    {
        if (angle > turretRotationAngle)
            turret.Rotate(transform.up, turretRotationAngle);
        else if (angle < -turretRotationAngle)
            turret.Rotate(transform.up, -turretRotationAngle);
        else
            turret.Rotate(turret.up, angle);
    }


    public void LiftMuzzle(float muzzleTurnAxis)
    {
        var turn = muzzleTurnAxis * muzzleRotationSpeed * Time.deltaTime;
        turretMuzzle.Rotate(Vector3.left * turn);
    }
}
