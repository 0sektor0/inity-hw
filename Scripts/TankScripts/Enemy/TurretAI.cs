﻿using UnityEngine;



[RequireComponent(typeof(Targeting))]
[RequireComponent(typeof(IShootable))]
class TurretAI : MonoBehaviour
{
    public float fov = 45;
    public Transform target;
    public Animator turretAnimator;
    private Targeting targeting;
    private IShootable ts;


    private void Start()
    {
        targeting = GetComponent<Targeting>();
        ts = GetComponent<IShootable>();
    }


    private void FixedUpdate()
    {
        if (target == null)
            return;

        if (IsSpoted())
        {
            targeting.AimTo(target.position);

            if (targeting.CanHit(target.gameObject))
                ts.Shoot();
        }
    }


    private bool IsSpoted()
    {
        float angle = targeting.CalcAngleToPoint(target.position);
        float distance = Vector3.Distance(transform.position, target.position);
        
        return angle < fov && angle > -fov && distance <= targeting.range;
    }
}
