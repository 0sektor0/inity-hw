﻿using UnityEngine;



[RequireComponent(typeof(Targeting))]
[RequireComponent(typeof(IShootable))]
[RequireComponent(typeof(Patroling))]
public class EnemyAI : MonoBehaviour
{
    public float chasingChance = 0.1f;
    public Transform target;
    public float fov = 45;
    private bool isChasing = false;
    private Targeting  targeting;
    private IShootable shootManager;
    private Patroling patroling;

    
    private void Start()
    {
        targeting = GetComponent<Targeting>();
        patroling = GetComponent<Patroling>();
        shootManager = GetComponent<IShootable>();
    }
    

    private void Update()
    {
        if (target == null)
            return;

        if (IsSpoted())
        {
            Chase();

            targeting.AimTo(target.position);

            if (targeting.CanHit(target.gameObject))
                shootManager.Shoot();
        }
        else if(isChasing)
        {
            isChasing = false;
            patroling.GotoNextPoint();
            return;
        }

        if (isChasing)
            patroling.Goto(target.position);
    }


    private bool IsSpoted()
    {
        float angle = targeting.CalcAngleToPoint(target.position);
        float distance = Vector3.Distance(transform.position, target.position);

        return angle < fov && angle > -fov && distance <= targeting.range;
    }


    private void Chase()
    {
        if (Random.value <= chasingChance)
            isChasing = true;
    }
}
