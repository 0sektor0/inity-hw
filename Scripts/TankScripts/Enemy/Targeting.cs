﻿using UnityEngine;



public class Targeting : MonoBehaviour
{
    public float range = 30;
    public Transform turret;
    public float aimStep = 1;
    private bool isAiming = false;
    private bool isTargeting = false;
    private Vector3 target;


    private void FixedUpdate()
    {
        Aim();
        Debug.DrawRay(turret.position, turret.forward * 100, Color.green);
    }


    private void SetTarget(Vector3 target, bool isTargeting)
    {
        this.isTargeting = isTargeting;
        this.target = target;
        isAiming = true;
    }


    public void LockAt(Vector3 t)
    {
        SetTarget(t, true);
    }


    public void AimTo(Vector3 t)
    {
        SetTarget(t, false);
    }


    private float CalcAngleToAim()
    {
        Vector3 vec = turret.transform.InverseTransformPoint(target);
        vec.y = 0;

        return Vector3.SignedAngle(Vector3.forward, vec, turret.up);
    }


    public float CalcAngleToPoint(Vector3 targetPosition)
    {
        Vector3 vec = turret.transform.InverseTransformPoint(targetPosition);
        vec.y = 0;

        return Vector3.SignedAngle(Vector3.forward, vec, turret.up);
    }


    private void Aim()
    {
        if (!isAiming || target == null)
            return;

        float angle = CalcAngleToAim();

        if (angle > aimStep)
            turret.Rotate(Vector3.up, aimStep);
        else if (angle < -aimStep)
            turret.Rotate(Vector3.up, -aimStep);
        else
        {
            turret.Rotate(Vector3.up, angle);

            if (!isTargeting)
            {
                isTargeting = false;
                isAiming = false;
            }
        }
    }


    public bool CanHit(GameObject obj)
    {
        Ray ray = new Ray(turret.position, turret.forward);
        RaycastHit hit;

        if (Physics.Raycast(turret.position, ray.direction, out hit, range))
        {
            Transform t = hit.collider.transform;
            while (t.parent != null)
                t = t.parent.transform;

            return t.gameObject == obj;
        }

        return false;
    }
}
