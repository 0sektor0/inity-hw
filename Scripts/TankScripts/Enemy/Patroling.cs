﻿using UnityEngine;
using UnityEngine.AI;



[RequireComponent(typeof(NavMeshAgent))]
public class Patroling : MonoBehaviour
{
    public Transform[] points;
    public bool isRandomPoints = false;
    private int destPoint = 0;
    private NavMeshAgent agent;


    private void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        agent.autoBraking = false;

        GotoNextPoint();
    }


    private void Update()
    {
        if (!agent.pathPending && agent.remainingDistance < agent.radius * 2)
            GotoNextPoint();
    }


    public void GotoNextPoint()
    {
        if (points.Length == 0)
            return;
        if (!isRandomPoints)
        {
            agent.destination = points[destPoint].position;
            destPoint = (destPoint + 1) % points.Length;
        }
        else
            agent.destination = points[Random.Range(0, points.Length)].position;
    }


    public void Goto(Vector3 dest)
    {
        agent.destination = dest;

        if (agent.remainingDistance < 15 * agent.radius)
            agent.isStopped = true;
        else
            agent.isStopped = false;
    }
}