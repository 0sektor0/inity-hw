﻿using UnityEngine;



[RequireComponent(typeof(TankMovements))]
public class TurretMouseControls : MonoBehaviour
{
    private TankMovements tm;


    protected void Start()
    {
        tm = GetComponent<TankMovements>();
        Debug.Log("TurretMouseControls started");
    }


    protected virtual void FixedUpdate()
    {
        LookAtMouse();
    }


    private void LookAtMouse()
    {
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit))
        {
            Vector3 dest = hit.point;
            dest = tm.turret.InverseTransformPoint(dest);
            dest.y = 0;

            var angle = Vector3.SignedAngle(Vector3.forward, dest, tm.turret.up);
            tm.RotateTurret(angle);

            //Debug.Log(dest.ToString() + " " + angle.ToString());
            Debug.DrawRay(tm.turret.position, new Vector3(ray.direction.x, 0, ray.direction.z) * hit.distance, Color.green);
            Debug.DrawRay(tm.turret.position, hit.distance * tm.turret.forward, Color.green);
        }
    }
}
