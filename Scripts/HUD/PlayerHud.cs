﻿using System.Collections;
using UnityEngine;



[RequireComponent(typeof(IWoundable))]
[RequireComponent(typeof(IShootable))]
[RequireComponent(typeof(TankMovements))]
class PlayerHud : MonoBehaviour
{
    private IEnumerator Start()
    {
        Gui gui = null;

        while (gui == null)
        {
            gui = GuiManager.singleton;
            yield return null;
        }

        gui.hud.SetPlayer(gameObject);
        Debug.Log("PlayerHud added");
    }
}
