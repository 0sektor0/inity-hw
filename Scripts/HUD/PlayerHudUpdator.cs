﻿using UnityEngine;
using UnityEngine.UI;



public class PlayerHudUpdator : MonoBehaviour
{
    public GameObject playableObject;
    public Text healthIndicator;
    public Text ammoIndicator;
    public Text muzzleRotationBar;
    public Image reloadIndicator;
    private TankMovements tm;
    private IShootable turret;
    private IWoundable health;

    private const float yellowIndicatorState = 0.35f;
    private const float greenIndicatorValue = 0.75f;
    private const float maxIndicatorValue = 1;


    public void SetPlayer(GameObject player)
    {
        playableObject = player;

        turret = playableObject.GetComponent<IShootable>();
        health = playableObject.GetComponent<IWoundable>();
        tm = playableObject.GetComponent<TankMovements>();
        ammoIndicator.color = Color.green;

        Debug.Log("PlayerHud started");
    }


    private void Start()
    {
        if (playableObject != null)
            SetPlayer(playableObject);
    }


    private void FixedUpdate()
    {
        if (playableObject == null)
            return;

        ShowHP();
        ShowAmmo();
        ShowReloading();
        //ShowMuzzleRotation();
    }


    private void ShowHP()
    {
        healthIndicator.text = string.Format("{0}/{1}", (int)health.GetHp(), (int)health.GetMaxHp());

        float hp = health.GetHpPercent();
        if (hp > greenIndicatorValue)
            healthIndicator.color = Color.green;
        else if (hp <= greenIndicatorValue && hp > yellowIndicatorState)
            healthIndicator.color = Color.yellow;
        else
            healthIndicator.color = Color.red;
    }


    private void ShowMuzzleRotation()
    {
        muzzleRotationBar.text = string.Format("Muzzle rotation: {0}", tm.turretMuzzle.rotation);
    }


    private void ShowAmmo()
    {
        ammoIndicator.text = string.Format("{0}", turret.GetAmmoCount());
    }


    private void ShowReloading()
    {
        float reloadingState = turret.GetReloadingPercent();
        reloadIndicator.fillAmount = reloadingState;

        reloadIndicator.rectTransform.position = Input.mousePosition;

        if (reloadingState < yellowIndicatorState)
            reloadIndicator.color = Color.red;
        else if(reloadingState < maxIndicatorValue)
            reloadIndicator.color = Color.yellow;
        else
            reloadIndicator.color = Color.green;
    }
}
