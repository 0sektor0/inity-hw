﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections;



[RequireComponent(typeof(IWoundable))]
public class OnHeadHealthBar : MonoBehaviour
{
    private RectTransform targetCanvas;
    private Transform objectToFollow;
    private IWoundable health;
    private Image onHeadHelathBar;
    private float verticalOffset = 50;
    

    private IEnumerator Start()
    {
        Gui gui = null;

        while (gui == null)
        {
            gui = GuiManager.singleton;
            yield return null;
        }

        targetCanvas = gui.canvasTransform;
        objectToFollow = GetComponent<Transform>();
        health = GetComponent<IWoundable>();
        AddHealthBarToCanvas();

        //Debug.Log("OnHead healthbar added");
    }


    private void OnEnable()
    {
        Start();
    }


    private void AddHealthBarToCanvas()
    {
        GameObject ngo = new GameObject("OnHeadHealthBar");
        ngo.transform.SetParent(targetCanvas);

        onHeadHelathBar = ngo.AddComponent<Image>();
        onHeadHelathBar.raycastTarget = false;
        onHeadHelathBar.rectTransform.sizeDelta = new Vector2(50, 10);
    }
    

    private void Update()
    {
        if (Camera.main == null || targetCanvas == null)
            return;

        ShowHp();
        RepositionHealthBar();
    }


    private void ShowHp()
    {
        float hp = health.GetHpPercent();
        if (hp > 0.75)
        {
            onHeadHelathBar.color = Color.green;
            onHeadHelathBar.rectTransform.localScale = new Vector3(hp, 1, 1);
        }
        else if (hp <= 0.75 && hp > 0.35)
        {
            onHeadHelathBar.color = Color.yellow;
            onHeadHelathBar.rectTransform.localScale = new Vector3(hp, 1, 1);
        }
        else
        {
            onHeadHelathBar.color = Color.red;
            onHeadHelathBar.rectTransform.localScale = new Vector3(hp, 1, 1);
        }
    }


    private void RepositionHealthBar()
    {
        Vector3 ViewportPosition = Camera.main.WorldToViewportPoint(objectToFollow.position);

        if (ViewportPosition.x < 0 || ViewportPosition.y < 0 || ViewportPosition.z < 0)
        {
            onHeadHelathBar.color = new Color(0, 0, 0, 0);
            return;
        }

        Vector3 WorldObject_ScreenPosition = new Vector3(
        ((ViewportPosition.x * targetCanvas.sizeDelta.x) - (targetCanvas.sizeDelta.x * 0.5f)),
        ((ViewportPosition.y * targetCanvas.sizeDelta.y) - (targetCanvas.sizeDelta.y * 0.5f) + verticalOffset),
        ViewportPosition.z);

        onHeadHelathBar.rectTransform.anchoredPosition = WorldObject_ScreenPosition;
    }
    

    private void OnDestroy()
    {
        if(onHeadHelathBar != null)
        Destroy(onHeadHelathBar.gameObject);
    }
}
