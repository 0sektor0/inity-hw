﻿using UnityEngine;



[RequireComponent(typeof(ITeamable))]
public class HealingPoint : MonoBehaviour
{
    public float healthSupply = 600;
	public float heatlingRate = 1;
	public float radius = 10;
    private int playersLayer;
    private ITeamable team;


    void Start()
    {
        team = GetComponent<ITeamable>();
        playersLayer = LayerMask.NameToLayer("Players");
    }


    void Update()
    {
		RaycastHit[] hits = Physics.SphereCastAll(transform.position, radius, Vector3.up, playersLayer);

		foreach(RaycastHit hit in hits)
		{
			IWoundable hp = hit.transform.gameObject.GetComponent<IWoundable>();
            if (hp != null)
                Heal(hp);
		}
    }


    protected virtual void Heal(IWoundable hp)
    {
        if (hp.GetHp() >= hp.GetMaxHp())
            return;

        float healing = heatlingRate * Time.deltaTime;

        if (healthSupply < healing)
            healing = healthSupply;

        hp.DealDamage(new Damage(-healing, team));
        healthSupply -= healing;

        if (healthSupply <= 0)
            Destroy(gameObject);
    }
}
