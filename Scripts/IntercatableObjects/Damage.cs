﻿using System;



public class Damage
{
    public float value;
    public ITeamable owner;
    

    public Damage(float value, ITeamable owner)
    {
        this.value = value;
        this.owner = owner;
    }
}