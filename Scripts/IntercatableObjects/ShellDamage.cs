﻿using UnityEngine;



//Компонент нанесения урона содержит в себе владельца снаряда 
//предполагается, что его можно использовать, как в сингле, так и в мультиплеере
public class ShellDamage : MonoBehaviour
{
    public ParticleSystem explosionPrefab;
    private bool isAlreadyinCollision = false;
    private Damage dmg;


    private void OnCollisionEnter(Collision collision)
    {
        IWoundable health = collision.gameObject.GetComponent<IWoundable>();

        if (isAlreadyinCollision)
        {
            Destroy(gameObject);
            return;
        }
        isAlreadyinCollision = true;

        if (health != null)
            health.DealDamage(dmg);

        SelfDestroy();
    }


    private void SelfDestroy()
    {
        Instantiate(explosionPrefab, transform.position, transform.rotation);
        Destroy(gameObject);
    }


    public void SetDamage(Damage dmg)
    {
        this.dmg = dmg;
    }
}
