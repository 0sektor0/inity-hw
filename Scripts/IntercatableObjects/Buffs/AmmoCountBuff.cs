﻿using System;
using UnityEngine;



[Serializable]
public class AmmoCountBuff : Buff
{
    [SerializeField] private int ammo = 20;
    private IShootable turret;


    public override bool ApplyBuff()
    {
        if (turret != null)
            turret.AddAmmo(ammo);

        return false;
    }


    public override void HangBuff(GameObject go)
    {
        turret = go.GetComponent<IShootable>();
    }
}