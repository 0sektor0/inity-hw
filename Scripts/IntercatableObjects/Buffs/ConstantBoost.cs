﻿using System;
using UnityEngine;



[Serializable]
public class ConstantBoost : Buff
{
    private TankMovements tm;
    [SerializeField] private float moveSpeedAddition = 5;
    [SerializeField] private float turnSpeedAddition = 20;
    [SerializeField] private float turretSpeedAddition = 0.5f;


    public override void HangBuff(GameObject go)
    {
        tm = go.GetComponent<TankMovements>();
    }


    public override bool ApplyBuff()
    {
        if (tm != null)
            tm.IncreaseMobility(moveSpeedAddition, turnSpeedAddition, turretSpeedAddition);
        else
            Debug.Log("tank movements dosent find");

        return false;
    }
}