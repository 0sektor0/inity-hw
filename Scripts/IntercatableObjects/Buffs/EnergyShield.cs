﻿using UnityEngine;



[System.Serializable]
public class EnergyShield : Buff
{
    private float disableTime;
    private IWoundable health;
    private bool isUsed = false;


    public override void HangBuff(GameObject go)
    {
        disableTime = Time.time + duration;

        health = go.GetComponent<IWoundable>();
        if (health == null)
        {
            disableTime = Time.time - 1;
            isUsed = false;
        }
    }


    public override bool ApplyBuff()
    {
        if (!IsAlive())
        {
            if (isUsed)
                health.SetInvulnerability(false);

            return false;
        }

        if (!isUsed)
        {
            health.SetInvulnerability(true);
            isUsed = true;
        }

        return true;
    }


    private bool IsAlive()
    {
        return disableTime - Time.time > 0;
    }
}
