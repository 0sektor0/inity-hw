﻿using System;
using UnityEngine;



[Serializable]
public abstract class Buff
{
    public float duration;
    protected float disabletime;

    abstract public bool ApplyBuff();

    abstract public void HangBuff(GameObject go);
}