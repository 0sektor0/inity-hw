﻿using System;
using UnityEngine;



[Serializable]
public class WeaponBuff : Buff
{
    [SerializeField] private float additionalDamage = 35;
    [SerializeField] private float additionalReloadingSpeed = 0.2f;
    private IShootable shootable;

    
    public override void HangBuff(GameObject go)
    {
        shootable = go.GetComponent<IShootable>();
    }


    public override bool ApplyBuff()
    {
        if(shootable != null)
        {
            shootable.AddDamage(additionalDamage);
            shootable.ReduceReloading(additionalReloadingSpeed);
        }

        return false;
    }
}