﻿using UnityEngine;



[System.Serializable]
public class TemporaryBoost : Buff
{
    private TankMovements tm;
    private float disableTime;
    [SerializeField] private float moveSpeedAddition = 5;
    [SerializeField] private float turnSpeedAddition = 5;
    [SerializeField] private float turretSpeedAddition = 0.1f;
    private bool isUsed = false;
    

    public override bool ApplyBuff()
    {
        if (!IsAlive())
        {
            if (isUsed)
                Buff(-1);

            return false;
        }

        if (!isUsed)
        {
            Buff(1);
            isUsed = true;
        }

        return true;
    }


    public override void HangBuff(GameObject go)
    {
        disableTime = Time.time + duration;

        tm = go.GetComponent<TankMovements>();
        if (tm == null)
        {
            disableTime = Time.time - 1;
            isUsed = false;
        }
    }


    private bool IsAlive()
    {
        return disableTime - Time.time > 0;
    }


    private void Buff(int rate)
    {
        tm.IncreaseMobility(rate * moveSpeedAddition, rate * turnSpeedAddition, rate * turretSpeedAddition);
    }
}
