﻿using UnityEngine;



public class BuffActivator<T> : MonoBehaviour where T : Buff 
{
    [SerializeField] protected T buff;
    [SerializeField] protected ParticleSystem particlesPrefab;


    protected virtual void OnTriggerEnter(Collider collider)
    {
        var buffManager = collider.gameObject.GetComponent<IBuffable>();

        if (buffManager != null)
        {
            Debug.Log("powerup taken");
            buff.HangBuff(collider.gameObject);
            buffManager.UseBuff(buff);

            if (particlesPrefab != null)
            {
                var particles = Instantiate(particlesPrefab, collider.gameObject.transform);
                particles.transform.parent = collider.gameObject.transform;

                var main = particles.main;
                main.duration = buff.duration;
                particles.Play();
            }
        }
        else
            Debug.Log("Buffmanager dosent find");


        Destroy(gameObject);
    }
}
